//
//  AVSMapViewController.h
//  AviasalesTest
//
//  Created by Максим Дорошенко on 16.02.17.
//  Copyright © 2017 Max Doroshenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AVSMapViewController : UIViewController
    
- (void)setAirport:(NSObject *)airport;
- (void)startAnimation;

@end
