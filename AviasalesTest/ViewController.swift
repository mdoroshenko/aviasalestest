//
//  ViewController.swift
//  AviasalesTest
//
//  Created by Максим Дорошенко on 15.02.17.
//  Copyright © 2017 Max Doroshenko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var airportButton: AVSButton!
    @IBOutlet weak var routeButton: AVSButton!
    
    var airport: AVSAirport?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let targetVC = segue.destination as? AVSAirportTableViewController else {
            return
        }
        targetVC.delegate = self
    }
    
    @IBAction func routeButtonClicked(_ sender: AnyObject) {
        let routeVC = AVSMapViewController.init(nibName: "AVSMapViewController", bundle: nil)
        routeVC.setAirport(airport)
        present(routeVC, animated: true, completion: { Void in routeVC.startAnimation()})
    }
}

extension ViewController: AVSAirportTableViewControllerDelegate {
    func airportSelected(airport: AVSAirport) {
        self.airport = airport
        guard let title = airport.airportName else {
            guard let title = airport.name else {
                updateAirport(title: "Неизвестный")
                return
            }
            updateAirport(title: title)
            return
        }
        updateAirport(title: title)
    }
    
    func updateAirport(title: String) {
        DispatchQueue.main.async {
            self.airportButton.setTitle(title, for: .normal)
            self.routeButton.isEnabled = true
            self.navigationController?.setNavigationBarHidden(true, animated: false)
        }
    }
}

