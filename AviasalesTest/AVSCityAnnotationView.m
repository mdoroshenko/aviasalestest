//
//  AVSCityAnnotationView.m
//  AviasalesTest
//
//  Created by Максим Дорошенко on 16.02.17.
//  Copyright © 2017 Max Doroshenko. All rights reserved.
//

#import "AVSCityAnnotationView.h"
#import "AviasalesTest-Swift.h"

@implementation AVSCityAnnotationView

- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        self.bounds = CGRectMake(0, 0, 64, 32);
        self.layer.borderColor = [UIColor avsMain].CGColor;
        self.layer.borderWidth = 2;
        self.layer.cornerRadius = 16;
        self.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.7];
        UILabel *title = [[UILabel alloc] initWithFrame:self.bounds];
        title.textAlignment = NSTextAlignmentCenter;;
        [title setFont:[UIFont systemFontOfSize:22.0]];
        title.textColor = [UIColor avsMain];
        self.titleLabel = title;
        [self addSubview:title];
        self.canShowCallout = NO;
    }
    return self;
}
    
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
