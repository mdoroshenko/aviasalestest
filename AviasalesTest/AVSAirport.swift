//
//  AVSAirport.swift
//  AviasalesTest
//
//  Created by Максим Дорошенко on 15.02.17.
//  Copyright © 2017 Max Doroshenko. All rights reserved.
//

import UIKit
import ObjectMapper
import MapKit


@objc class AVSAirport: NSObject, Mappable {
    var name: String?
    var airportName: String?
    var lat: Double?
    var lon: Double?
    var iata: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        airportName <- map ["airport_name"]
        lat <- map["location.lat"]
        lon <- map["location.lon"]
        iata <- map["iata"]
    }

    func coordinates() -> CLLocationCoordinate2D {
        guard let lat = lat, let lon = lon else {
            return kCLLocationCoordinate2DInvalid;
        }
        return CLLocationCoordinate2DMake(lat, lon)
    }
}
