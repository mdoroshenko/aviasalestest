//
//  AVSColors.swift
//  AviasalesTest
//
//  Created by Максим Дорошенко on 15.02.17.
//  Copyright © 2017 Max Doroshenko. All rights reserved.
//

import UIKit

extension UIColor {
    class func avsMain() -> UIColor {
        return UIColor(colorLiteralRed: 0.08, green: 0.63, blue: 0.84, alpha: 1)
    }
}
