//
//  AVSMapViewController.m
//  AviasalesTest
//
//  Created by Максим Дорошенко on 16.02.17.
//  Copyright © 2017 Max Doroshenko. All rights reserved.
//

#import "AVSMapViewController.h"
#import <MapKit/MapKit.h>
#import "AviasalesTest-Swift.h"
#import "AVSCityAnnotationView.h"

#define kAnimSpeed 10.0
#define kAirplaneTitle @"AIRPLANE"

@interface AVSMapViewController () <MKMapViewDelegate>
    
@property (strong, nonatomic) AVSAirport *destination;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) MKPointAnnotation *airplane;
    
@end

@implementation AVSMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.mapView.delegate = self;
    [self putPins];
    [self makeRoute];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
- (IBAction)closeButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
    
- (void)setAirport:(NSObject *)airport {
    if ([airport isKindOfClass:[AVSAirport class]]) {
        self.destination = (AVSAirport *)airport;
    }
}
    
- (void)startAnimation {
    [self makeAnimation];
}
    
- (void)putPins {
    MKPointAnnotation *stPet = [self startPoint];
    MKPointAnnotation *dest = [self endPoint];
    NSArray *annotations = @[stPet, dest, self.airplane];
    [self.mapView addAnnotations:annotations];
    [self.mapView showAnnotations:self.mapView.annotations animated:NO];
}
    
- (void)makeRoute {
    CLLocationCoordinate2D coords[2];
    coords[0] = [self startPoint].coordinate;
    coords[1] = [self endPoint].coordinate;
    MKPolyline *line = [MKPolyline polylineWithCoordinates:coords count:2];
    [self.mapView addOverlay:line];
}
    
- (void)makeAnimation {
    [UIView animateWithDuration:kAnimSpeed animations:^{
        self.airplane.coordinate = self.destination.coordinates;
    }];
}
- (MKPointAnnotation *)airplane {
    if (!_airplane) {
        MKPointAnnotation *airplane = [[MKPointAnnotation alloc] init];
        airplane.coordinate = [self startPointAirplane];
        airplane.title = kAirplaneTitle;
        _airplane = airplane;
    }
    return _airplane;
}
    
- (MKPointAnnotation *)startPoint {
    MKPointAnnotation *stPet = [[MKPointAnnotation alloc] init];
    stPet.coordinate = [self startPointCoordinates];
    stPet.title = @"LED";
    return stPet;
}
    
- (CLLocationCoordinate2D)startPointCoordinates {
    return CLLocationCoordinate2DMake(59.925903, 30.298115);
}
    
- (CLLocationCoordinate2D)startPointAirplane {
    return CLLocationCoordinate2DMake(59.92590, 30.5);
}
    
- (MKPointAnnotation *)endPoint {
    MKPointAnnotation *dest = [[MKPointAnnotation alloc] init];
    dest.coordinate = self.destination.coordinates;
    dest.title = self.destination.iata;
    return dest;
}
    
#pragma mark - MKMapViewDelegate
- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.strokeColor = [UIColor avsMain];
    renderer.lineWidth = 6;
    renderer.lineDashPattern = @[@8, @12];
    return renderer;
}
    
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    static NSString *airplaneIdentifier = @"airplane";
    static NSString *pinIdentifier = @"pin";
    if ([annotation isKindOfClass:[MKPointAnnotation class]]) {
        MKPointAnnotation *ann = (MKPointAnnotation *)annotation;
        if ([ann.title isEqualToString:kAirplaneTitle]) {
            MKAnnotationView *view = [mapView dequeueReusableAnnotationViewWithIdentifier:airplaneIdentifier];
            if (!view) {
                view = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:airplaneIdentifier];
            }
            view.image = [UIImage imageNamed:@"plane"];
            view.canShowCallout = NO;
            [self rotateAirplane:view startCoordinates:[self startPointCoordinates] destination:self.destination.coordinates];
            return view;
        } else {
            AVSCityAnnotationView *view = (AVSCityAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:pinIdentifier];
            if (!view) {
                view = [[AVSCityAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:pinIdentifier];
            }
            view.titleLabel.text = ann.title;
            return view;
        }
    } else {
        return [[MKAnnotationView alloc] initWithFrame:CGRectZero];
    }
}
    
- (void)rotateAirplane:(MKAnnotationView *)plane startCoordinates:(CLLocationCoordinate2D)startCoords destination:(CLLocationCoordinate2D)endCoords {
    
    CGPoint from = [self.mapView convertCoordinate:startCoords toPointToView:self.view];
    CGPoint to = [self.mapView convertCoordinate:endCoords toPointToView:self.view];
    
    CGFloat angle = atan2(from.y - to.y, from.x - to.x);
    [plane setTransform:CGAffineTransformRotate(CGAffineTransformIdentity, angle)];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
