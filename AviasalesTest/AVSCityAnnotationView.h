//
//  AVSCityAnnotationView.h
//  AviasalesTest
//
//  Created by Максим Дорошенко on 16.02.17.
//  Copyright © 2017 Max Doroshenko. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface AVSCityAnnotationView : MKAnnotationView
    
@property (weak, nonatomic) UILabel *titleLabel;

@end
