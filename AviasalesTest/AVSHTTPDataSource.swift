//
//  AVSHTTPDataSource.swift
//  AviasalesTest
//
//  Created by Максим Дорошенко on 15.02.17.
//  Copyright © 2017 Max Doroshenko. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class AVSHTTPDataSource: NSObject {
    
    static let url = "http://places.aviasales.ru/places?locale=ru&term="
    
    class func airports(title: String?, handler: @escaping ([AVSAirport]?) -> Void) {
        guard let title = title , title.characters.count > 2 else {
            return
        }
        let queryString = url + title
        Alamofire.request(queryString).responseArray { (response: DataResponse<[AVSAirport]>) in
            handler(response.result.value)
        }
    }

}
