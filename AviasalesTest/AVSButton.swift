//
//  AVSButton.swift
//  AviasalesTest
//
//  Created by Максим Дорошенко on 15.02.17.
//  Copyright © 2017 Max Doroshenko. All rights reserved.
//

import UIKit

class AVSButton: UIButton {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        layer.borderWidth = 2
        layer.cornerRadius = 22
        layer.borderColor = UIColor.avsMain().cgColor
        setTitleColor(UIColor.avsMain(), for: .normal)
    }

}
